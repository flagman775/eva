module Settings
  class NavBar < Setting
    attribute :data, :json
    store_accessor :data, :items

    class Item
      include ActiveModel::Attributes
      attribute :title, :string, default: nil
      attribute :link, :string, default: nil
      attribute :format_type, default: 'dynamic'
    end

    def default_data
      { items: [Item.new.attributes] }
    end

    private

    def check_data_format
      setting_data = super || default_data
      if setting_data.present? && items.present?
        data_items = items.is_a?(String) ? JSON.parse(items) : items
        check_title(data_items)
        check_link(data_items)
      else
        self.items = nil
        errors.add(:items, 'is wrong data format')
      end
    end

    def check_link(items)
      case items
      when Array
        items.map do |item|
          if item.is_a?(Hash) && item.key?('link')
            format_link(item)
          else
            errors.add(:items, 'Link is not present in an Array')
          end
        end
      when Hash
        if items.key?('link')
          format_link(items)
        else
          errors.add(:items, 'Link is not present in a Hash')
        end
      end
      items
    end

    def check_title(items)
      case items
      when Array
        items.map do |item|
          errors.add(:items, 'Title is not present in an Array') if item.is_a?(Hash) && (!item.key?('title') || !item['title'].present?)
        end
      when Hash
        errors.add(:items, 'Title is not present in a Hash') if !items.key?('title') && !items['title'].present?
      end
    end

    def format_link(item)
      if item['link'].present?
        item['link'].chr == '/' ? item : item['link'].insert(0, '/')
      else
        item['link'] = '/'
      end
      item
    end
  end
end
