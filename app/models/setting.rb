class Setting < ApplicationRecord
  attribute :name, :string
  attribute :description, :string
  attribute :data, :string

  validates :name, presence: true
  validates :name, uniqueness: true
  validates :data, presence: true
  validate  :check_data_format

  after_initialize :initialize_defaults, if: :new_record?

  def default_name
    setting_class_name == 'setting' ? "setting_#{setting_number}" : setting_class_name
  end

  def default_description
    nil
  end

  def default_data
    nil
    # Available formats for the Setting Form template:
    # { items: [{ key_0: 'value_0', key_1: 'value_1'}] }
    # { items: [{ key_0: 'value_0', key_1: 'value_1' }, 'element_1'] }
    # { items: ['element_0', 'element_1'] }
    # { items: { key_0: 'value_0', key_1: 'value_1'} }
    # { items: 'element_0' }
  end

  def get_default_data(name)
    name.present? && (name.start_with?('setting_') == false) && Object.const_defined?(object_class) ? object_class.constantize.new.default_data : default_data
  end

  def object_class
    "Settings/#{name}".classify
  end

  private

  def initialize_defaults
    self.name ||= default_name
    self.data ||= default_data
    self.description ||= default_description
  end

  def setting_number
    ((Setting.last.present? ? Setting.last.id : 0) + 1).to_s
  end

  def setting_class_name
    self.class.to_s.demodulize.underscore
  end

  def check_data_format; end
end
