# frozen_string_literal: true

class ApplicationController < ActionController::Base
  before_action :load_navbar

  def load_navbar
    setting = Settings::NavBar.where(name: 'nav_bar').last
    navbar_data = setting&.data && setting.data['items'] ? setting.data['items'] : {}
    return unless navbar_data.present? && navbar_data.is_a?(Array)

    @navbar = navbar_data.each_with_index.map do |item, index|
      { name: item['title'], img: item['img'], href: item['link'], current: index.zero? }
    end
  end
end
