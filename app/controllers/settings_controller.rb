class SettingsController < ApplicationController
  before_action :new_setting, only: %i[new create edit updete], if: -> { @setting }
  rescue_from ActionController::UnpermittedParameters, with: :handle_errors

  def index
    @settings = Setting.all.order(:id, :name).to_a
    setting_names = Setting.pluck(:name)
    Setting.subclasses.each do |setting|
      name = setting.to_s.demodulize.underscore
      @settings << Setting.new(name:) unless setting_names.include?(name.to_s)
    end
  end

  def new
    @setting = Setting.new
    if params[:setting_name]
      @setting.name = params[:setting_name]
      @setting.data = @setting.get_default_data(params[:setting_name])
    end
    render 'new', locals: { setting: generate_setting(@setting) }
  end

  def create
    @setting = generate_setting(Setting.new(setting_params))
    respond_to do |format|
      if @setting.valid? && @setting.save
        format.html { redirect_to action: 'index' }
        format.json { render :index, status: :ok }
      else
        format.html { redirect_to new_setting_path(@setting, setting_name: @setting.name) }
        format.json { render json: new_setting.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    @setting = Setting.find_by(id: params[:id])
    render 'edit', locals: { setting: generate_setting(@setting) }
  end

  def update
    @setting = generate_setting(Setting.find_by(id: params[:id]))
    respond_to do |format|
      if @setting.valid? && @setting.update(setting_params)
        format.html { redirect_to action: 'index' }
        format.json { render :index, status: :ok }
      else
        format.html { redirect_to edit_setting_url(@setting, setting_name: @setting.name) }
        format.json { render json: setting.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @setting = Setting.find_by(id: params[:id]).destroy
    redirect_to action: 'index'
  end

  private

  def setting_params
    if params.key?(:db)
      params.require(:setting).permit(:name, :description, :new, :data)
    else
      params.require(:setting).permit(:name, :description, :new, data: {})
    end
  end

  def generate_setting(object = nil)
    action_params = params[:setting].present? ? setting_params : nil
    if object.new_record?
      Setting.subclasses.include?(setting_class(object)) ? setting_class(object).new(action_params) : object
    elsif params[:id]
      Setting.subclasses.include?(setting_class(object)) ? setting_class(object).find_by(id: params[:id]) : object
    end
  end

  def setting_class(setting = nil)
    if setting && (defined? setting.object_class) && Object.const_defined?(setting.object_class)
      setting.object_class.constantize
    elsif setting.is_a?(String)
      "Settings::#{setting.classify}".constantize
    end
  end
end
