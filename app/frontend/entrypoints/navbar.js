import { createApp } from 'vue/dist/vue.esm-bundler.js';
import NavBar from '../components/NavBar.vue';

let element = document.getElementById('navbar-menu')
let navbarMenuItems = JSON.parse(element.getAttribute('data-navbar')).navbar

const navbar = createApp(NavBar, {
  navbarMenuItems: navbarMenuItems
})

navbar.mount('#navbar-menu')
