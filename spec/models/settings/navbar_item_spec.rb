require 'rails_helper'

RSpec.describe Settings::NavBar, type: :model do
  let(:navbar) { build(:navbar) }

  describe 'Settings::NavBar' do
    it 'Title should be present' do
      navbar.validate
      expect(navbar.errors[:items]).to include('Title is not present in an Array')
    end

    it 'Link should be present' do
      navbar.validate
      navbar.data['items'].each do |item|
        expect(item.with_indifferent_access[:link]).to start_with('/')
      end
    end
  end
end
