FactoryBot.define do
  factory(:navbar, class: 'Settings::NavBar') do
    {
      data: {
        items: [{
          title: nil,
          link: nil
        }]
      }
    }
  end
end
